/**
 * @param {Array} collection
 * @params {Function[]} – Функции для запроса
 * @returns {Array}
 */


function query(collection) {

    // Нужные промежуточные переменные
    var SELECT_QUERY = [];
    var FILTER_QUERY = {};
    var SELECT_LIST = [];
    var FILTER_LIST = [];
    var SELECT_COUNT = {};
    var FILTER_COUNT_FIRST = {};
    var FILTER_COUNT_SECOND = {};
    var FILTER_COUNTER = {};
    var TOTAL = [];

    // Подготавливаем аргументы
    var args = [].slice.call(arguments);
    args.shift();

    // Если кроме коллекции нет аргументов, то возвращаем исходную коллекцию
    if (args.length == 0) {
        //console.log('Нет аргументов');
        return collection.slice();

        // иначе смотрим, какие аргументы пришли и запоминаем их
    } else {
        var selectCounter = 0;
        var filterCounter = 0;
        for (var i = 0; i < args.length; i++) {
            var arg = args[i];
            if (arg[0] == 'filterIn') {
                filterCounter += 1;
                var filterUnit = arg[1];
                FILTER_LIST.push(filterUnit);
            }
            if (arg[0] == 'select') {
                selectCounter += 1;
                var selectUnit = arg[1];
                SELECT_LIST.push(selectUnit);
            }
        }

        //console.log(selectCounter);
        //console.log(filterCounter);

        // Определяем содежимое фильтров
        for (var m = 0; m < FILTER_LIST.length; m++) {
            var filterUnit = FILTER_LIST[m];
            var filterType = filterUnit[0];
            var filterValues = filterUnit[1];

            // Складываем фильтры по типу и считаем количество применений каждого фильтра (и запоминаем его)
            if (FILTER_COUNT_FIRST.hasOwnProperty(filterType)) {
                FILTER_COUNT_FIRST[filterType] = FILTER_COUNT_FIRST[filterType].concat(filterValues);
                FILTER_COUNTER[filterType] += 1;
            } else {
                FILTER_COUNT_FIRST[filterType] = filterValues;
                FILTER_COUNTER[filterType] = 1;
            }
        }

        // Преодразуем список фильтров в удобный формат
        for (var filterType in FILTER_COUNT_FIRST) {
            FILTER_COUNT_SECOND[filterType] = {};
            var filterValue = FILTER_COUNT_FIRST[filterType];
            for (var n = 0; n < filterValue.length; n++) {
                var filterUnit = filterValue[n];
                if (FILTER_COUNT_SECOND[filterType].hasOwnProperty(filterUnit)) {
                    FILTER_COUNT_SECOND[filterType][filterUnit] += 1;
                } else {
                    FILTER_COUNT_SECOND[filterType][filterUnit] = filterUnit;
                    FILTER_COUNT_SECOND[filterType][filterUnit] = 1;
                }
            }
        }

        // Считаем пересечение фильтров
        var totalFilter = [];
        for (var filterType in FILTER_COUNT_SECOND) {
            var filterCount = FILTER_COUNT_SECOND[filterType];
            for (var filterUnit in filterCount) {
                if (filterCount[filterUnit] == FILTER_COUNTER[filterType]) {
                    totalFilter.push(filterUnit);
                    FILTER_QUERY[filterType] = totalFilter;

                }
            }
            totalFilter = [];
        }

        //console.log(FILTER_QUERY);
        //console.log(Object.keys(FILTER_QUERY).length);

        // Если пересечение фильтров пустое, то возвращаем пустой массив
        if ((Object.keys(FILTER_QUERY) == 0) & (filterCounter > 0)) {
            return [];
        }

        // Складываем select-ы по типу и считаем количество применений каждого поля
        for (var j = 0; j < SELECT_LIST.length; j++) {
            var selectUnit = SELECT_LIST[j];
            selectUnit = unique(selectUnit);
            for (var k = 0; k < selectUnit.length; k++) {
                var inst = selectUnit[k];
                if (SELECT_COUNT.hasOwnProperty(inst)) {
                    SELECT_COUNT[inst] += 1;
                } else {
                    SELECT_COUNT[inst] = 1;
                }
            }
        }

        // Считаем пересечение select-ов
        for (var selectInst in SELECT_COUNT) {
            if (SELECT_COUNT[selectInst] == SELECT_LIST.length) {
                SELECT_QUERY.push(selectInst);
            }
        }

        //console.log(SELECT_QUERY);

        // Если пересечение select-ов пустое, то возвращаем исходную коллекцию
        if ((SELECT_QUERY.length == 0) & (selectCounter > 0)) {
            //console.log('Нет пересечения селектов');
            return [];
        }

        // фильтруем коллекцию
        let deepCopiedCollection = deepCopyFunction(collection)
        for (var a = 0; a < deepCopiedCollection.length; a++) {
            var entry = deepCopiedCollection[a];
            var bools = [];

            // Если есть фильтры
            if (filterCounter > 0) {
                for (var filterInst in FILTER_QUERY) {
                    if (entry.hasOwnProperty(filterInst)) {
                        if (FILTER_QUERY[filterInst].includes(entry[filterInst])) {
                            bools.push(true);
                        } else {
                            bools.push(false);
                        }
                    }
                }
                var boolCounter = 0;
                for (var b = 0; b < bools.length; b++) {
                    var boolFilter = bools[b];
                    if (boolFilter == true) {
                        boolCounter += 1;
                    }
                }

                // выбираем поля по фильтру (удаляем в копии коллекции те поля, которых нет в select)
                if (boolCounter == bools.length) {
                    if (selectCounter > 0) {
                        for (var prop in entry) {
                            if (SELECT_QUERY.includes(prop) == false) {
                                delete entry[prop];
                            }
                        }
                    }
                    TOTAL.push(entry);
                }

            // Если нет фильтров
            } else {
                for (var prop in entry) {
                    if (SELECT_QUERY.includes(prop) == false) {
                        delete entry[prop];
                    }
                }
                TOTAL.push(entry);
            }
        }
        // Возвращаем новую коллекцию
        return TOTAL;
    }
}

function select() {
    var fields = [].slice.call(arguments);
    return ['select', fields];
}

function filterIn(property, values) {
    var fields = [].slice.call(arguments);
    return ['filterIn', fields];
}

const deepCopyFunction = inObject => {
    let outObject, value, key;

    if(typeof inObject !== "object" || inObject === null) {
        return inObject // Return the value if inObject is not an object
    }

    // Create an array or object to hold the values
    outObject = Array.isArray(inObject) ? [] : {}

    for (key in inObject) {
        value = inObject[key]

        // Recursively (deep) copy for nested objects, including arrays
        outObject[key] = (typeof value === "object" && value !== null) ? deepCopyFunction(value) : value
    }

    return outObject
}

function unique(arr) {
    let result = [];
    for (let str of arr) {
        if (!result.includes(str)) {
            result.push(str);
        }
    }
    return result;
}

module.exports = {
    query: query,
    select: select,
    filterIn: filterIn
};
