/**
 * @param {String} date
 * @returns {Object}
 */
module.exports = function (date) {
    var time = new Date(date);
    time.setMinutes(time.getMinutes() - time.getTimezoneOffset());
    var possibleValues = ['years', 'months', 'days', 'hours', 'minutes'];
    return {
        add: function (count, value) {
            if ((possibleValues.indexOf(value) === -1) || (count < 1)) {
                throw new TypeError;
            };
            switch (value) {
                case 'years':
                    time.setFullYear(time.getFullYear() + count);
                    break;
                case 'months':
                    time.setMonth(time.getMonth() + count);
                    break;
                case 'days':
                    time.setDate(time.getDate() + count);
                    break;
                case 'hours':
                    time.setHours(time.getHours() + count);
                    break;
                case 'minutes':
                    time.setMinutes(time.getMinutes() + count);
                    break;
            };
            return this;
        },
        subtract: function (count, value) {
            if ((possibleValues.indexOf(value) === -1) || (count < 1)) {
                throw new TypeError;
            };
            switch (value) {
                case 'years':
                    time.setFullYear(time.getFullYear() - count);
                    break;
                case 'months':
                    time.setMonth(time.getMonth() - count);
                    break;
                case 'days':
                    time.setDate(time.getDate() - count);
                    break;
                case 'hours':
                    time.setHours(time.getHours() - count);
                    break;
                case 'minutes':
                    time.setMinutes(time.getMinutes() - count);
                    break;
            };
            return this;
        },
        get value() {
            return time.toISOString().replace('T', ' ').replace(':00.000Z', '');
        }
    }
}
