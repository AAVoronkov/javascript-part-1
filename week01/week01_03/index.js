/**
 * @param {Number} hours
 * @param {Number} minutes
 * @param {Number} interval
 * @returns {String}
 */
module.exports = function (hours, minutes, interval) {
    var timeSum, newHour, newMinutes, result;

    if (interval > 1440) {
        interval = interval % 1440;
    }

    timeSum = hours * 60 + minutes + interval;

    if (timeSum < 1440) {
        newHour = Math.floor((timeSum / 60));
        if (newHour <= 9) {
            newHour = '0' + String(newHour);
        } else {
            newHour = String(newHour);
        }
        newMinutes = timeSum - newHour * 60;
        if (newMinutes <= 9) {
            newMinutes = '0' + String(newMinutes);
        } else {
            newMinutes = String(newMinutes);
        }
        result = newHour + ':' + newMinutes;
        return result;

    } else {
        timeSum = timeSum - 1440;
        newHour = Math.floor((timeSum / 60));
        if (newHour <= 9) {
            newHour = '0' + String(newHour);
        } else {
            newHour = String(newHour);
        }
        newMinutes = timeSum - newHour * 60;
        if (newMinutes <= 9) {
            newMinutes = '0' + String(newMinutes);
        } else {
            newMinutes = String(newMinutes);
        }
        result = String(newHour) + ':' + newMinutes;
        return result;
    }
};
