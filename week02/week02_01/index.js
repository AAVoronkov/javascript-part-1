/**
 * @param {String} tweet
 * @returns {String[]}
 */
module.exports = function (tweet) {
    var result = [];
    var splittedTweet;

    splittedTweet = tweet.split(' ');

    for (var i = 0; i < splittedTweet.length; i++) {
        var word = splittedTweet[i];
        if (word.startsWith('#')) {
            var hashtag = word.slice(1);
            result.push(hashtag);
        };
    };
    return result;
};
