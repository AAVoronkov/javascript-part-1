module.exports = {

    subscribers: [],

    /**
     * @param {String} event
     * @param {Object} subscriber
     * @param {Function} handler
     */

    on: function (event, subscriber, handler) {

        this.subscribers.push({
            nameOfEvent: event,
            subscriber: subscriber,
            handler: handler
        });
        return this;
    },

    /**
     * @param {String} event
     * @param {Object} subscriber
     */

    off: function (event, subscriber) {

        var deleteIndex = -1;
        this.subscribers.findIndex(function (array, index) {
            if (array.nameOfEvent == event && array.subscriber === subscriber) {
                deleteIndex = index;
                return index;
            }
        });

        if (deleteIndex != -1) {
            this.subscribers.splice(deleteIndex, 1);
            return this.off(event, subscriber);
        } else {
            return this;
        }
    },

    /**
     * @param {String} event
     */

    emit: function (event) {

        for (var j = 0; j < this.subscribers.length; j++) {
            var subscriber = this.subscribers[j];
            if (subscriber.nameOfEvent === event
                && subscriber.subscriber != undefined
                && subscriber.handler != undefined) {
                subscriber.handler.call(subscriber.subscriber);
            }
        }
        return this;
    }
};
