// Телефонная книга
var phoneBook = {};

/**
 * @param {String} command
 * @returns {*} - результат зависит от команды
 */

module.exports = function (command) {
    var commandName = command.split(' ')[0];

    if (commandName === 'ADD') {
        var contactName = command.split(' ')[1];
        var phoneNumber = command.split(' ')[2];

        if (phoneNumber.indexOf(',')) {
            phoneNumber = phoneNumber.split(',').join(', ');
        };

        if (phoneBook.hasOwnProperty(contactName)) {
            phoneBook[contactName] = phoneBook[contactName] + ', ' + phoneNumber;
        } else {
            phoneBook[contactName] = phoneNumber;
        };
        return true;
    };

    if (commandName === 'SHOW') {
        var result = [];
        var keys = Object.keys(phoneBook);

        for (var i = 0; i < keys.length; i++) {
            var key = keys[i];
            var value = phoneBook[key];
            var notation = key + ': ' + value;
            result.push(notation);
        }
        return result.sort();
    };

    if (commandName === 'REMOVE_PHONE') {
        var removeResult = false;
        var phoneNumber = command.split(' ')[1];
        var keys = Object.keys(phoneBook);

        for (var i = 0; i < keys.length; i++) {
            var key = keys[i];
            var value = phoneBook[key];
            var valueNew = value.split(', ').filter(number => number != phoneNumber).join(', ');
            if (value != valueNew) {
                phoneBook[key] = valueNew;
                if (valueNew == '') {
                    delete phoneBook[key];
                };
                removeResult = true;
            };
        };
        return removeResult;
    };
};
